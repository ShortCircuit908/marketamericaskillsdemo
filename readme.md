#SkillsDemo

##How to build this project
1. Configure `~/.gradle/gradle.properties` with the following values:
    * `RELASE_STORE_FILE`: This should point to your keystore
    * `RELEASE_STORE_PASSWORD`: Your keystore's password
    * `RELEASE_KEY_ALIAS`: Name of the key used to sign the final APK
    * `RELEASE_KEY_PASSWORD` The key's password
2. In your favorite shell utility, run `gradlew assembleRelase` from the project root


##Design Choices
* Although I have somewhat limited experience creating Android applications, I chose to do so because I am most fluent
in Java and am most comfortable working with JetBrains IDEs. In this case, I chose to use IntelliJ IDEA over Android
Studio simply to avoid installing another piece of software.
* To navigate the app's few choices, I used a nav drawer for its visual appeal. The drawer provides a convenient
location for navigation buttons without blocking the main views.
* To meet the requirement of using one of the phone's native features, I chose to use the camera. There is no reason for
this choice other than that the phone I have to test with does not include a fingerprint scanner or other now-common
sensors. I chose to let the app access an external camera application rather than creating my own. While this limits the
ability to tailor the camera experience to the needs of the app, I feel it is best to present the user with an interface
that they know and feel comfortable using.
* In order to access the Shop.com API, I wrote a small set of helper classes to abstract the querying process. This is
especially helpful because it makes the process of adding more API endpoints trivial and helps ensure the validity of
the queries. It also allows for queries to be created and handled in a standardized way.
* As this application is only a demonstration, I chose to keep the design simple. For example, I limited the product
search to 50 results. In a proper application, pagination of the results is a trivial task, but for this purpose it is
more than what is necessary to demonstrate the usage of the API.