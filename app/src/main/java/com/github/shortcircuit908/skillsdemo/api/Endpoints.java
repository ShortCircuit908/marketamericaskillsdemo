package com.github.shortcircuit908.skillsdemo.api;

import java.util.Objects;

/**
 * @author Caleb Milligan
 * Created on 6/29/2018.
 */
public enum Endpoints {
	GET_CATEGORIES("categories", QueryParameters.API_KEY, QueryParameters.PUBLISHER_ID, QueryParameters.LOCALE),
	GET_SEARCH_BY_TERM("products", QueryParameters.API_KEY, QueryParameters.PUBLISHER_ID, QueryParameters.LOCALE, QueryParameters.SEARCH_TERM),
	GET_PRODUCT("products", QueryParameters.API_KEY, QueryParameters.PUBLISHER_ID, QueryParameters.LOCALE, QueryParameters.PRODUCT_ID);
	
	public static final String BASE_URI = "api2.shop.com/AffiliatePublisherNetwork/v2/";
	
	private final String base_uri;
	private final String endpoint;
	private final QueryParameters[] allowed_parameters;
	
	Endpoints(String endpoint, QueryParameters... allowed_parameters) {
		this(BASE_URI, endpoint, allowed_parameters);
	}
	
	Endpoints(String base_uri, String endpoint, QueryParameters... allowed_parameters) {
		this.base_uri = base_uri;
		this.endpoint = endpoint;
		this.allowed_parameters = allowed_parameters;
	}
	
	public String getBaseUri() {
		return base_uri;
	}
	
	public String getEndpoint() {
		return endpoint;
	}
	
	public QueryParameters[] getAllowedParameters() {
		return allowed_parameters;
	}
	
	public boolean allowsParameter(QueryParameters parameter) {
		// No query parameters specified for this endpoint
		if (allowed_parameters == null) {
			return false;
		}
		for (QueryParameters search_parameter : allowed_parameters) {
			if (Objects.equals(search_parameter, parameter)) {
				return true;
			}
		}
		return false;
	}
	
	public enum QueryParameters {
		API_KEY("api_key"),
		LOCALE("locale"),
		PUBLISHER_ID("publisherId"),
		SEARCH_TERM("term"),
		PRODUCT_ID(null, true);
		
		private final String key;
		private final boolean append_to_uri;
		
		QueryParameters(String key) {
			this(key, false);
		}
		
		QueryParameters(String key, boolean append_to_uri) {
			this.key = key;
			this.append_to_uri = append_to_uri;
		}
		
		public String getKey() {
			return key;
		}
		
		public boolean appendToUri() {
			return append_to_uri;
		}
	}
}
