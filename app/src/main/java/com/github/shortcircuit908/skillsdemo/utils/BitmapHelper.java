package com.github.shortcircuit908.skillsdemo.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;

/**
 * @author Caleb Milligan
 * Created on 7/1/2018.
 */
public class BitmapHelper {
	public static int findAppropriateSampleSizeForScaling(BitmapFactory.Options options, int desired_width, int desired_height) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int sample_size = 1;
		if (height > desired_height || width > desired_width) {
			// Increase sample size until resulting dimensions fit within the specified values
			final int half_height = height / 2;
			final int half_width = width / 2;
			while (((half_height) / sample_size) >= desired_height && (half_width / sample_size) >= desired_width) {
				sample_size *= 2;
			}
		}
		return sample_size;
	}
	
	public static Bitmap decodeSampledBitmap(File file, int desired_width, int desired_height) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(file.getAbsolutePath(), options);
		options.inSampleSize = findAppropriateSampleSizeForScaling(options, desired_width, desired_height);
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
	}
	
	public static Bitmap decodeSampledBitmap(byte[] data, int desired_width, int desired_height) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(data, 0, data.length, options);
		options.inSampleSize = findAppropriateSampleSizeForScaling(options, desired_width, desired_height);
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(data, 0, data.length, options);
	}
}
