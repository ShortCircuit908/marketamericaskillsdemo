package com.github.shortcircuit908.skillsdemo.api;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Caleb Milligan
 * Created on 7/6/2018.
 */
public class APIResponse {
	private final JSONObject raw_response;
	private final int response_code;
	
	protected APIResponse(JSONObject response) throws JSONException {
		this.raw_response = response;
		this.response_code = response.has("status") ? response.getInt("status") : 200;
	}
	
	public JSONObject getRawResponse() {
		return raw_response;
	}
	
	public int getResponseCode() {
		return response_code;
	}
	
	public boolean failed() {
		return response_code < 200 || response_code > 304;
	}
	
	public String getFailureCause() {
		return getFailureItem("cause");
	}
	
	public String getFailureCode() {
		return getFailureItem("code");
	}
	
	public String getFailureMessage() {
		return getFailureItem("message");
	}
	
	private String getFailureItem(String key) {
		try {
			return raw_response.getString(key);
		}
		catch (JSONException e) {
			// Do nothing
		}
		return null;
	}
}
