package com.github.shortcircuit908.skillsdemo.api;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Caleb Milligan
 * Created on 6/29/2018.
 */
public class APIQuery {
	private final URL url;
	
	private APIQuery(URL url) {
		this.url = url;
	}
	
	public URL getUrl() {
		return url;
	}
	
	public APIResponse fetchResponse() throws IOException, JSONException {
		// Open connection
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		// Get input stream as a reader
		InputStreamReader reader = new InputStreamReader(
				connection.getResponseCode() >= 200 && connection.getResponseCode() <= 300
						? connection.getInputStream()
						: connection.getErrorStream()
		);
		// Read string data from stream
		StringBuilder builder = new StringBuilder();
		char[] buffer = new char[1024];
		int read;
		while ((read = reader.read(buffer)) > 0) {
			builder.append(buffer, 0, read);
		}
		// Close connections
		reader.close();
		connection.disconnect();
		// Tokenize and return response
		String response = builder.toString();
		return new APIResponse(new JSONObject(response));
	}
	
	public static class Builder {
		private Endpoints endpoint;
		private LinkedHashMap<Endpoints.QueryParameters, Object> query_parameters = new LinkedHashMap<>();
		
		public Builder() {
		
		}
		
		public Builder setEndpoint(Endpoints endpoint) {
			// Set endpoint and clear any previously set parameters
			this.endpoint = endpoint;
			query_parameters.clear();
			return this;
		}
		
		public Builder setQueryParameter(Endpoints.QueryParameters parameter, Object value) {
			if (endpoint == null) {
				throw new IllegalStateException("Endpoint must be set");
			}
			if (!endpoint.allowsParameter(parameter)) {
				throw new IllegalArgumentException(String.format("Endpoint %1$s does not allow parameter %2$s", endpoint, parameter));
			}
			query_parameters.put(parameter, value);
			return this;
		}
		
		public APIQuery build() {
			if (endpoint == null) {
				throw new IllegalStateException("Endpoint must be set");
			}
			// Initialize string builder for the base URL
			StringBuilder url_builder = new StringBuilder("https://")
					.append(endpoint.getBaseUri());
			if (url_builder.charAt(url_builder.length() - 1) != '/') {
				url_builder.append('/');
			}
			url_builder.append(endpoint.getEndpoint());
			// Initialize string builder for the params string
			StringBuilder params_builder = new StringBuilder("?");
			for (Map.Entry<Endpoints.QueryParameters, Object> entry : query_parameters.entrySet()) {
				// Encode key for use as a query parameter
				String encoded_value;
				try {
					encoded_value = URLEncoder.encode(Objects.toString(entry.getValue()), "UTF-8");
				}
				catch (UnsupportedEncodingException e) {
					// This shouldn't happen
					throw new AssertionError("UTF-8 is somehow not supported");
				}
				if (entry.getKey().appendToUri()) {
					// Some parameters, such as PRODUCT_ID, are appended to the URL path instead of being
					// included as a query parameter
					if (url_builder.charAt(url_builder.length() - 1) != '/') {
						url_builder.append('/');
					}
					url_builder.append(encoded_value);
				}
				else {
					// Append query parameter as a key/value pair
					params_builder.append(entry.getKey().getKey())
							.append('=').
							append(encoded_value)
							.append('&');
				}
			}
			// Strip trailing ampersand
			params_builder.deleteCharAt(params_builder.length() - 1);
			// Append query params to URL
			url_builder.append(params_builder);
			try {
				return new APIQuery(new URL(url_builder.toString()));
			}
			catch (MalformedURLException e) {
				// This should never happen
				throw new AssertionError("Malformed URL in hardcoded enum");
			}
		}
	}
}
