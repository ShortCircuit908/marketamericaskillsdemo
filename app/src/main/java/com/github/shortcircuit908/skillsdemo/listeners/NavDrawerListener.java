package com.github.shortcircuit908.skillsdemo.listeners;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import com.github.shortcircuit908.skillsdemo.MainActivity;
import com.github.shortcircuit908.skillsdemo.R;

/**
 * @author Caleb Milligan
 * Created on 7/7/2018.
 */
public class NavDrawerListener implements NavigationView.OnNavigationItemSelectedListener {
	private final MainActivity context;
	
	public NavDrawerListener(MainActivity context) {
		this.context = context;
	}
	
	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem item) {
		int id = item.getItemId();
		
		// Switch displayed view
		if (id == R.id.nav_camera) {
			context.bringViewToFront(context.getViewImage());
			context.dispatchTakePictureIntent();
		}
		else if (id == R.id.nav_search) {
			context.bringViewToFront(context.getViewSearch());
		}
		
		// Close the drawer
		DrawerLayout drawer = context.findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}
}
