package com.github.shortcircuit908.skillsdemo.listeners;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import com.github.shortcircuit908.skillsdemo.ItemDescriptor;
import com.github.shortcircuit908.skillsdemo.MainActivity;
import com.github.shortcircuit908.skillsdemo.R;
import com.github.shortcircuit908.skillsdemo.tasks.SearchTask;
import com.github.shortcircuit908.skillsdemo.utils.HtmlUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Caleb Milligan
 * Created on 7/6/2018.
 */
public class SearchQueryListener implements SearchView.OnQueryTextListener {
	private final MainActivity context;
	
	public SearchQueryListener(MainActivity context) {
		this.context = context;
	}
	
	@Override
	public boolean onQueryTextSubmit(String query) {
		// Gather views
		final ListView list_view = context.getViewSearch().findViewById(R.id.search_results);
		@SuppressWarnings("unchecked") final ArrayAdapter<ItemDescriptor> adapter = (ArrayAdapter<ItemDescriptor>) list_view.getAdapter();
		// Execute search asynchronously
		new SearchTask(new SearchTask.Callback() {
			@Override
			public void onPostExecute(JSONObject result) {
				try {
					// Extract the product list
					JSONArray products = result.getJSONArray("products");
					// Cap the results at 50 for performance
					// Since this is only a demo, I elected not to implement pagination
					final int display_count = Math.min(products.length(), 50);
					adapter.clear();
					// Add each product to the ArrayAdapter
					for (int i = 0; i < display_count; i++) {
						JSONObject product = products.getJSONObject(i);
						String unescaped = HtmlUtils.unescapeHtml(product.getString("name"));
						
						adapter.add(new ItemDescriptor(
								unescaped,
								product.getInt("id")
						));
					}
					// Finalize changes
					adapter.notifyDataSetChanged();
					Toast.makeText(context, String.format("Displaying %1$s results of %2$s", display_count, products.length()), Toast.LENGTH_SHORT).show();
				}
				catch (JSONException e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
			}
		}).execute(new SearchTask.Params(context.getString(R.string.api_key), query));
		return true;
	}
	
	@Override
	public boolean onQueryTextChange(String newText) {
		return true;
	}
}
