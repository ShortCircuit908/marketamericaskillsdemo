package com.github.shortcircuit908.skillsdemo.tasks;

import android.os.AsyncTask;
import android.util.Log;
import com.github.shortcircuit908.skillsdemo.api.APIQuery;
import com.github.shortcircuit908.skillsdemo.api.APIResponse;
import com.github.shortcircuit908.skillsdemo.api.Endpoints;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Caleb Milligan
 * Created on 7/7/2018.
 */
public class SearchTask extends AsyncTask<SearchTask.Params, Void, JSONObject> {
	private final Callback callback;
	
	public SearchTask(Callback callback) {
		this.callback = callback;
	}
	
	@Override
	protected JSONObject doInBackground(Params... params) {
		// Create and execute query
		APIQuery query = new APIQuery.Builder()
				.setEndpoint(Endpoints.GET_SEARCH_BY_TERM)
				.setQueryParameter(Endpoints.QueryParameters.PUBLISHER_ID, "test")
				.setQueryParameter(Endpoints.QueryParameters.API_KEY, params[0].api_key)
				.setQueryParameter(Endpoints.QueryParameters.LOCALE, "en_US")
				.setQueryParameter(Endpoints.QueryParameters.SEARCH_TERM, params[0].query)
				.build();
		try {
			final APIResponse response = query.fetchResponse();
			return response.getRawResponse();
		}
		catch (IOException | JSONException e) {
			Log.e("Error", e.getMessage());
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(JSONObject result) {
		callback.onPostExecute(result);
	}
	
	public interface Callback {
		void onPostExecute(JSONObject result);
	}
	
	public static class Params {
		private final String api_key;
		private final String query;
		
		public Params(String api_key, String query) {
			this.api_key = api_key;
			this.query = query;
		}
	}
}
