package com.github.shortcircuit908.skillsdemo.listeners;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.shortcircuit908.skillsdemo.ItemDescriptor;
import com.github.shortcircuit908.skillsdemo.MainActivity;
import com.github.shortcircuit908.skillsdemo.R;
import com.github.shortcircuit908.skillsdemo.tasks.DetailsTask;
import com.github.shortcircuit908.skillsdemo.tasks.ImageFetcherTask;
import com.github.shortcircuit908.skillsdemo.utils.HtmlUtils;
import java.io.IOException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Caleb Milligan
 * Created on 7/7/2018.
 */
public class ProductDetailsListener implements AdapterView.OnItemClickListener {
	private final MainActivity context;
	
	public ProductDetailsListener(MainActivity context) {
		this.context = context;
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		@SuppressWarnings("unchecked")
		ArrayAdapter<ItemDescriptor> adapter = (ArrayAdapter<ItemDescriptor>) parent.getAdapter();
		ItemDescriptor descriptor = adapter.getItem(position);
		if (descriptor == null) {
			return;
		}
		fetchProductInfo(descriptor.getProductId());
		context.bringViewToFront(context.getViewDetails());
	}
	
	private void fetchProductInfo(int product_id) {
		new DetailsTask(new DetailsTask.Callback() {
			@Override
			public void onPostExecute(JSONObject result) {
				try {
					// Collect views
					final View view = context.getViewDetails();
					final ImageView view_details_image = view.findViewById(R.id.details_image);
					final TextView view_details_seller = view.findViewById(R.id.details_seller);
					final TextView view_details_brand = view.findViewById(R.id.details_brand);
					final TextView view_details_price = view.findViewById(R.id.details_price);
					final TextView view_details_caption = view.findViewById(R.id.details_caption);
					final TextView view_details_description = view.findViewById(R.id.details_description);
					
					// Set basic product information
					view_details_seller.setText(result.has("catalogName") ? "Sold by " + HtmlUtils.unescapeHtml(result.getString("catalogName")) : "");
					view_details_brand.setText(result.has("brand") ? HtmlUtils.fromHtml(result.getString("brand")) : "");
					view_details_caption.setText(HtmlUtils.fromHtml(result.getString("name")));
					view_details_description.setText(HtmlUtils.fromHtml(result.getString("description")));
					
					// Extract price info
					JSONArray object_options = result.getJSONArray("options");
					String price_string = object_options.getJSONObject(0).getString("priceString");
					view_details_price.setText(price_string);
					
					// Extract the image with the largest dimensions from the array of available images
					JSONObject object_images = result.getJSONObject("image");
					JSONArray object_images_sizes = object_images.getJSONArray("sizes");
					JSONObject max_size = null;
					int max_size_value = 0;
					for (int i = 0; i < object_images_sizes.length(); i++) {
						JSONObject size = object_images_sizes.getJSONObject(i);
						int size_value = size.getInt("width") * size.getInt("height");
						if (max_size == null || max_size_value < size_value) {
							max_size = size;
							max_size_value = size_value;
						}
					}
					
					// Handle a product with no images associated with it
					if (max_size == null) {
						view_details_image.setImageResource(android.R.drawable.picture_frame);
					}
					else {
						// Fetch the image asynchronously
						new ImageFetcherTask(new ImageFetcherTask.Callback() {
							@Override
							public void onPostExecute(Bitmap result) {
								view_details_image.setImageBitmap(result);
							}
						}).execute(new ImageFetcherTask.Params(new URL(max_size.getString("url")), 600, 600));
					}
				}
				catch (IOException | JSONException e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
			}
		}).execute(new DetailsTask.Params(context.getString(R.string.api_key), product_id));
	}
}
