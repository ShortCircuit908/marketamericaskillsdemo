package com.github.shortcircuit908.skillsdemo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import com.github.shortcircuit908.skillsdemo.listeners.NavDrawerListener;
import com.github.shortcircuit908.skillsdemo.listeners.ProductDetailsListener;
import com.github.shortcircuit908.skillsdemo.listeners.SearchQueryListener;
import com.github.shortcircuit908.skillsdemo.utils.BitmapHelper;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
	private View view_image;
	private View view_details;
	private View view_search;
	private static final int request_image_capture = 1;
	private File last_image_file;
	
	@Override
	protected void onCreate(Bundle saved_instance_state) {
		super.onCreate(saved_instance_state);
		setContentView(R.layout.activity_main);
		
		// Attach listener to nav drawer
		NavigationView nav_view = findViewById(R.id.nav_view);
		nav_view.setNavigationItemSelectedListener(new NavDrawerListener(this));
		
		// Open drawer by default
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.openDrawer(GravityCompat.START);
		
		// Inflate views
		view_image = getLayoutInflater().inflate(R.layout.content_image, null);
		view_search = getLayoutInflater().inflate(R.layout.content_search, null);
		view_details = getLayoutInflater().inflate(R.layout.content_details, null);
		
		// Init the search bar
		SearchView search_view = view_search.findViewById(R.id.search_view);
		search_view.setIconifiedByDefault(false);
		search_view.setSubmitButtonEnabled(true);
		search_view.setOnQueryTextListener(new SearchQueryListener(this));
		
		// Init the search result list
		ListView list_view = view_search.findViewById(R.id.search_results);
		list_view.setAdapter(new ArrayAdapter<ItemDescriptor>(this, android.R.layout.simple_list_item_1));
		list_view.setOnItemClickListener(new ProductDetailsListener(this));
	}
	
	public View getViewImage() {
		return view_image;
	}
	
	public View getViewDetails() {
		return view_details;
	}
	
	public View getViewSearch() {
		return view_search;
	}
	
	@Override
	public void onBackPressed() {
		// Close the drawer if it is open
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			super.onBackPressed();
		}
	}
	
	public void bringViewToFront(View view) {
		// Swap out the main displayed view
		FrameLayout layout = findViewById(R.id.view_container);
		layout.removeAllViews();
		layout.addView(view);
	}
	
	public void dispatchTakePictureIntent() {
		Intent intent_take_photo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		// Ensure there is an available camera activity
		if (intent_take_photo.resolveActivity(getPackageManager()) != null) {
			try {
				last_image_file = createImageFile();
			}
			catch (IOException ex) {
				Toast.makeText(this, R.string.error_cannot_create_file, Toast.LENGTH_SHORT).show();
				return;
			}
			// Dispatch camera activity with defined output path
			Uri uri = FileProvider.getUriForFile(this, "com.github.shortcircuit908.skillsdemo.fileprovider", last_image_file);
			intent_take_photo.putExtra(MediaStore.EXTRA_OUTPUT, uri);
			startActivityForResult(intent_take_photo, request_image_capture);
		}
	}
	
	@Override
	protected void onActivityResult(int request_code, int result_code, Intent data) {
		if (request_code == request_image_capture && result_code == RESULT_OK) {
			// Scale and display captured image
			ImageView image_view = findViewById(R.id.image_view_photo);
			Bitmap bitmap = BitmapHelper.decodeSampledBitmap(last_image_file, image_view.getWidth(), image_view.getHeight());
			image_view.setImageBitmap(bitmap);
		}
	}
	
	private File createImageFile() throws IOException {
		@SuppressLint("SimpleDateFormat") String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String name = getTitle() + "_" + timestamp + "_";
		return File.createTempFile(name, ".jpg", getExternalFilesDir(Environment.DIRECTORY_PICTURES));
	}
}
