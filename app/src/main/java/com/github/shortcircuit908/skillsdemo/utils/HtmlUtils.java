package com.github.shortcircuit908.skillsdemo.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * @author Caleb Milligan
 * Created on 7/7/2018.
 */
public class HtmlUtils {
	public static String unescapeHtml(String str) {
		return fromHtml(str).toString();
	}
	
	public static Spanned fromHtml(String str) {
		return fromHtml(str, 0);
	}
	
	public static Spanned fromHtml(String str, int flags) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			return Html.fromHtml(str, flags);
		}
		else {
			return Html.fromHtml(str);
		}
	}
}
