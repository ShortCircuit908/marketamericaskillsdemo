package com.github.shortcircuit908.skillsdemo.tasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import com.github.shortcircuit908.skillsdemo.utils.BitmapHelper;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

/**
 * @author Caleb Milligan
 * Created on 7/7/2018.
 */
public class ImageFetcherTask extends AsyncTask<ImageFetcherTask.Params, Void, Bitmap> {
	private final Callback callback;
	
	public ImageFetcherTask(Callback callback) {
		this.callback = callback;
	}
	
	protected Bitmap doInBackground(Params... params) {
		try {
			// Read data into a byte array
			InputStream in = params[0].url.openStream();
			ByteArrayOutputStream out = new ByteArrayOutputStream(in.available());
			byte[] buffer = new byte[1024];
			int read;
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			// Extract the raw bytes
			byte[] data = out.toByteArray();
			// Scale the image
			return BitmapHelper.decodeSampledBitmap(data, params[0].desired_width, params[0].desired_height);
		}
		catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		callback.onPostExecute(result);
	}
	
	public interface Callback {
		void onPostExecute(Bitmap result);
	}
	
	public static class Params {
		private final URL url;
		private final int desired_width;
		private final int desired_height;
		
		public Params(URL url, int desired_width, int desired_height) {
			this.url = url;
			this.desired_width = desired_width;
			this.desired_height = desired_height;
		}
	}
}
