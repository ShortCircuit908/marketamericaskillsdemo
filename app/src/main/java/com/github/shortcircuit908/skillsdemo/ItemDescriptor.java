package com.github.shortcircuit908.skillsdemo;

/**
 * @author Caleb Milligan
 * Created on 7/6/2018.
 */
public class ItemDescriptor {
	private final String name;
	private final int product_id;
	
	public ItemDescriptor(String name, int product_id) {
		this.name = name;
		this.product_id = product_id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getProductId() {
		return product_id;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
