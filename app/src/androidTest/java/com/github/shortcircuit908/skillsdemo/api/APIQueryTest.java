package com.github.shortcircuit908.skillsdemo.api;

import android.util.Log;
import java.io.IOException;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Caleb Milligan
 * Created on 6/29/2018.
 */
public class APIQueryTest {
	@Test
	public void fetchResponse() throws IOException, JSONException {
		APIQuery query = new APIQuery.Builder()
				.setEndpoint(Endpoints.GET_PRODUCT)
				.setQueryParameter(Endpoints.QueryParameters.PRODUCT_ID, 1505973330)
				.setQueryParameter(Endpoints.QueryParameters.LOCALE, "en_US")
				.setQueryParameter(Endpoints.QueryParameters.API_KEY, "d51f396500f94271b6686510e02ea6b4")
				.setQueryParameter(Endpoints.QueryParameters.PUBLISHER_ID, "test")
				.build();
		Log.d("SkillsDemo", query.getUrl().toString());
		APIResponse response = query.fetchResponse();
		Assert.assertNotNull(response);
		Log.d("SkillsDemo", response.getRawResponse().toString());
	}
}